<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<table>
	<thead>
		<tr>
			<td>Nombre</td>
			<td>Editar</td>
			<td>Borrar</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="permission" items="${listPermissions}">
			<tr>
				<input type="hidden" name="permissionId${permission.id}" value="${permission.id }" />
				<td>${permission.name}</td>
				<td><button name="btnEditPermission${permission.id}" onclick="getPermissionForm(${permission.id}, 'edit')">Editar</button></td>
				<td><button name="btnDeletePermission${permission.id}" onclick="removePermission(${permission.id})">Eliminar</button></td>
			</tr>
		</c:forEach>
	</tbody>
</table>