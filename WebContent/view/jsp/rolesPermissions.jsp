<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<h1>Permisos asignados a: ${rol.name }</h1>

<form action="/SpringSecurity/rol/addPermission.html" id="permissionsFormSelect" method="post">
	<select name="permissionsList" id="permissionsList">
	<option value="none" selected disabled>Seleciona un permiso </option>
	<c:forEach var="permission" items="${permissions}" >
		<option value="${permission.id}">${permission.name}</option>
	</c:forEach>
	</select>
</form>
<button id="btnAddPermissionRol" >Agregar permiso</button>

<table>
	<thead>
		<tr>
			<td>Rol</td>
			<td>Permiso</td>
			<td>Borrar</td>
		</tr>
	</thead>
	<tbody id="tablePermissionsByRol">
		<c:forEach var="permission" items="${permissionsByRol}">
			<tr>
				<input type="hidden" name="rol" value="${rol.id}" id="rolId"/>
				<td>${rol.name }</td>
				<td>${permission.name}</td>
				<td><button name="btnDeleteRol${permission.id}" onclick="removeRolPermission(${rol.id},${permission.id})">Eliminar</button></td>
			</tr>
		</c:forEach>
	</tbody>
</table>