<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div id="divPerson" >
		<form:form id="formPerson" modelAttribute="person" action="/SpringSecurity/person/${action}.html" method="post">
			<fieldset>
			<h1>${action} persona</h1>
				<div class="col-md-12">
					<label>Nombre:</label> 
					<form:input type="text"  path="name" placeholder="Ingrese el nombre del usuario" value="${person.name !=null ? person.name : ''}" ></form:input>
				</div>

				<div class="col-md-12">
					<label>Apellido:</label> 
					<form:input type="text" placeholder="Ingrese el apellido" path="firstName" value="${person.firstName !=null ? person.firstName : ''}" ></form:input>
				</div>

				<div class="col-md-12">
					<label>Edad: </label> 
					<form:input type="number" placeholder="Ingrese la edad" path="age" value="${person.age !=null ? person.age : ''}"></form:input>
				</div>
				<div class="col-md-12">
					<label>Correo electrónico:</label> 
					<form:input type="email" placeholder="Ingrese el correo electrónico" path="email" value="${person.email !=null ? person.email : ''}" ></form:input>
				</div>
				<div class="col-md-12">
					<label>Fecha de Nacimiento:</label> 
						<form:input type="text" placeholder="Ingrese la fecha de nacimiento" path="birthDate" value="${person.birthDate !=null ? person.birthDate : ''}" ></form:input> <%-- pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" --%>
				</div>
				<form:input type="hidden" path="registeredDate" value="${person.registeredDate !=null ? person.registeredDate : ''}"/>
				<form:input type="hidden" path="id" value="${person.id !=null ? person.id : ''}"/>
				<form:input type="hidden" path="password" value="${person.password !=null ? person.password : ''}"/>		
				<input id="btnSavePerson" value="Guardar" type="submit"/>
			</fieldset>
		</form:form>
	</div>