<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div id="divPerson" >
		<form:form id="formPermission" modelAttribute="permission" action="/SpringSecurity/permission/${action}.html" method="post">
			<fieldset>
			<h1>${action} permiso</h1>
				<div class="col-md-12">
					<label>Nombre:</label> 
					<form:input type="text"  path="name" placeholder="Ingrese el nombre del permiso" value="${permission.name !=null ? permission.name : ''}" ></form:input>
				</div>
				<form:input type="hidden" path="id" value="${permission.id !=null ? permission.id : ''}"/>
				<input id="btnSavePermission" value="Guardar" type="submit"/>
			</fieldset>
		</form:form>
</div>