<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.example.model.Person"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="/WEB-INF/tld/fmt.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/themes/jquery-ui-1.12.1.base/jquery-ui.css" />
<!-- <link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/index.css" /> -->
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/bootstrap/bootstrap.min.css" />
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/bootstrap/bootstrap-datepicker.css"/>
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/validationEngine.jquery.css" />
<link rel="stylesheet" type="text/css" href="/SpringSecurity/view/js/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" href="/SpringSecurity/view/js/jquery.dataTables.min.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script type="text/javascript" src="/SpringSecurity/view/js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.ui.core.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.dataTables.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.dataTables.min.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.validationEngine-es.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.validationEngine.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/bootstrap/bootstrap.js"></script>
<script type="text/javascript" src="/SpringSecurity/view/js/bootstrap-datepicker.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/bootstrap-datepicker.min.js" /></script>
<script type="text/javascript" charset="ISO-8859-1" src="/SpringSecurity/view/js/index.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery-3.4.1.js"></script>
  	<!-- Simple Sidebar -->
  	<link href="/SpringSecurity/view/css/simple-sidebar.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<script type="text/javascript" src="/SpringSecurity/view/js/persons.js"></script>
<script type="text/javascript" src="/SpringSecurity/view/js/permissions.js"></script>
<script type="text/javascript" src="/SpringSecurity/view/js/roles.js"></script>
<title>Inicio</title>
</head>
<body  class="hold-transition skin-blue sidebar-mini" onload="startLoad()">
<!-- Simple sidebar -->

<!-- Simple sidebar -->
  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-dark border-right text-white" id="sidebar-wrapper">
      <div class="sidebar-heading">Mi App </div>
      <div class="list-group list-group-flush">
        <a class="list-group-item list-group-item-action bg-dark">Personas</a>
        <a id="btnGetPersons" onclick="getPersons()" class="list-group-item list-group-item-action bg-dark text-white" href="">Listar personas</a>
        <a id="btnNewPerson" onclick="getPersonForm(null, 'add')" class="list-group-item list-group-item-action bg-dark text-white" href="">Nueva persona</a>
        <hr>
        <a class="list-group-item list-group-item-action bg-dark">Roles</a>
        <a id="btnGetRoles" onclick="getPersons()" class="list-group-item list-group-item-action bg-dark text-white" href="">Listar roles</a>
        <a id="btnNewRol" onclick="getRolForm(null, 'add')" class="list-group-item list-group-item-action bg-dark text-white" href="">Nuevo rol</a>
        <hr>
        <a class="list-group-item list-group-item-action bg-dark">Permisos</a>
        <a id="btnGetPermissions" onclick="getPersons()" class="list-group-item list-group-item-action bg-dark text-white" href="">Listar permisos</a>
        <a id="btnNewPermission" onclick="getPermissionForm(null, 'add')" class="list-group-item list-group-item-action bg-dark text-white" href="">Nuevo permiso</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-info border-bottom">
        <button class="btn btn-outline-light" id="menu-toggle"><i class="material-icons">menu</i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown text-white">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Bienvenido ${user.name }
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item">
                  	<small>${pageContext.request.userPrincipal.name}</small>
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<c:url value='/j_spring_security_logout'/>">Cerrar sesi�n</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <h1 class="mt-4">T�tulo</h1>
        <div id="mainPanel">
        
	        <div id="tablePersons"></div>
			<div id="tableRolesByPerson"></div>
			<div id="formPerson"></div>
			
			<div id="tableRoles"></div>
			<div id="tablePermissionsByRol"></div>
			<div id="formRol"></div>
			
			<div id="tablePermissions"></div>
			<div id="formPermission"></div>
        
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->
  
  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
</body>
</html>