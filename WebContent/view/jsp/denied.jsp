<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Acceso denegado</title>
</head>
<body>
<h1>Acceso denegado</h1>
	<c:if test="${not empty error}">
	    <div style="color:red">
	        Your fake login attempt was bursted, dare again !!<br /> 
	        Caused : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
	    </div>
	</c:if>
</body>
</html>