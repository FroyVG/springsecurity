<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div id="divPerson" >
		<form:form id="formRol" modelAttribute="personRol" action="/SpringSecurity/personRol/${action}.html" method="post">
			<fieldset>
			<h1>${action} roles por persona</h1>
				<div class="col-md-12">
					<label>Personas:</label> 
					<form:input type="text"  path="name" placeholder="Ingrese el nombre del rol" value="${rol.name !=null ? rol.name : ''}" ></form:input>
				</div>
				<div class="col-md-12">
					<label>Roles:</label> 
					<form:input type="text"  path="name" placeholder="Ingrese el nombre del rol" value="${rol.name !=null ? rol.name : ''}" ></form:input>
				</div>
				<form:input type="hidden" path="id" value="${rol.id !=null ? rol.id : ''}"/>
				<input id="btnSavePersonRol" value="Guardar" type="submit"/>
			</fieldset>
		</form:form>
</div>