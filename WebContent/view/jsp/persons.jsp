<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<table>
	<thead>
		<tr>
			<td>Nombre</td>
			<td>Apellido</td>
			<td>Correo</td>
			<td>Edad</td>
			<td>Fecha de nacimiento</td>
			<td>Ver roles</td>
			<td>Editar</td>
			<td>Borrar</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="person" items="${listPersons}">
			<tr>
				<input type="hidden" name="personId" value="${person.id }" />
				<td>${person.name}</td>
				<td>${person.firstName}</td>
				<td>${person.email}</td>
				<td>${person.age}</td>
				<td>${person.birthDate}</td>
				<td><button name="btnGetRolesByPerson${person.id}" onclick="getRolesByPerson(${person.id})">Ver roles</button>
				<td><button name="btnEditPerson${person.id}" onclick="getPersonForm(${person.id}, 'edit')">Editar</button></td>
				<td><button name="btnDeletePerson${person.id}" onclick="removePerson(${person.id})">Eliminar</button></td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<div id="formPersonRol"></div>