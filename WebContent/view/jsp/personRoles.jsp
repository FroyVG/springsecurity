<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<h1>Roles asignados a: ${person.name }</h1>

<form action="/SpringSecurity/person/addRol.html" id="rolesFormSelect" method="post">
	<select name="rolesList" id="rolesList">
	<option value="none" selected disabled>Seleciona un rol </option>
	<c:forEach var="rol" items="${roles}" >
		<option value="${rol.id}">${rol.name}</option>
	</c:forEach>
	</select>
	
</form>
<button id="btnAddRolPerson" >Agregar permiso</button>

<table>
	<thead>
		<tr>
			<td>Persona</td>
			<td>Rol</td>
			<td>Borrar</td>
		</tr>
	</thead>
	<tbody id="tableRolesByPerson">
		<c:forEach var="rol" items="${rolesByPerson}">
			<tr>
				<input type="hidden" name="person" value="${person.id}" id="personId"/>
				<td>${person.name }</td>
				<td>${rol.name}</td>
				<td><button name="btnDeleteRol${rol.id}" onclick="removePersonRol(${person.id},${rol.id})">Eliminar</button></td>
			</tr>
		</c:forEach>
	</tbody>
</table>