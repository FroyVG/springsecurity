<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Inicio de sesi�n</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/themes/jquery-ui-1.12.1.base/jquery-ui.css" />
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/index.css" />
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/bootstrap/bootstrap.min.css" />
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/bootstrap/bootstrap-datepicker.css" />
<link rel="styleSheet" type="text/css" href="/SpringSecurity/view/css/validationEngine.jquery.css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script type="text/javascript" src="/SpringSecurity/view/js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.ui.core.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.validationEngine-es.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/jquery.validationEngine.js" /></script>
<script type="text/javascript" src="/SpringSecurity/view/js/bootstrap/bootstrap.js"></script>
  <!-- Simple Sidebar -->
  <link href="/SpringSecurity/view/css/simple-sidebar.css" rel="stylesheet">
<script type="text/javascript" charset="ISO-8859-1" src="/SpringSecurity/view/js/index.js" /></script>
<style type="text/css">
.login-form {
	width: 340px;
	margin: 50px auto;
}

.login-form form {
	margin-bottom: 15px;
	background: #f7f7f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}

.login-form h2 {
	margin: 0 0 15px;
	color: black;
}

.form-control, .btn {
	min-height: 38px;
	border-radius: 2px;
}

.input-group-addon .fa {
	font-size: 18px;
}

.btn {
	font-size: 15px;
	font-weight: bold;
	margin-top: 15px;
}
</style>
</head>
<body class="hold-transition login-page" style="background-color: #17a2b8 !important; color: white !important; margin-top:25px !important;">

		<div class="w-25 p-3 mx-auto">
			<c:if test="${not empty error}">
				<div class="alert alert-danger" role="alert">
				  ${error}
				</div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="alert alert-success" role="alert">
				  ${msg}
				</div>
			</c:if>		
		</div>

		<div class="box box-success login-form">
			<c:url var="urlLogin" value="j_spring_security_check"/>	
			<form:form action="${urlLogin}" method="post" modelAttribute="loginDto" id="">
				<h2 class="text-center">Ingrese sus datos de acceso</h2>
				<hr>
				<div>
					<label for="validationTooltip03" style="color: black;">Correo electr�nico:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="validationTooltipAgePrepend"><i
								class="material-icons">mail</i></span>
						</div>
						<form:input type="email"  path="email"
							class="form-control validate[required], custom[email], minSize[2], maxSize[100]"
							name="email" id="email" placeholder="Ingrese correo electr�nico" value="${loginDto.email }"/>
					</div>
				</div>
				<div>
					<label for="validationTooltip01" style="color: black;">Contrase�a:
					</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="validationTooltipNamePrepend"><i
								class="material-icons">vpn_key</i></span>
						</div>
						<form:input type="password" path="password"
							class="form-control validate[required], minSize[2], maxSize[100]"
							name="password" id="pass" placeholder="Ingrese contrase�a" value="${loginDto.password}"
							/>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-outline-primary btn-block">Iniciar sesi�n</button>
				</div>
			</form:form>
		</div>

</body>
</html>