<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<table>
	<thead>
		<tr>
			<td>Nombre</td>
			<td>Asignar permiso</td>
			<td>Editar</td>
			<td>Borrar</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="rol" items="${listRoles}">
			<tr>
				<input type="hidden" name="rolId${rol.id}" value="${rol.id }" />
				<td>${rol.name}</td>
				<td><button name="btnAssignPermission" onclick="getPermissionsByRol(${rol.id})">Ver permisos</button>
				<td><button name="btnEditRol${rol.id}" onclick="getRolForm(${rol.id}, 'edit')">Editar</button></td>
				<td><button name="btnDeleteRol${rol.id}" onclick="removeRol(${rol.id})">Eliminar</button></td>
			</tr>
		</c:forEach>
	</tbody>
</table>