/**
 * 
 */

function startLoad() {
	//$("table").dataTable();
	
	$("#btnSavePerson").click(function() {
		console.log("Guardar persona");
		var form = $("#formPerson");
		$.ajax({
			url : form.attr('action'),
			type : form.attr('method'),
			data : form.serialize(),
			success : function(result) {
				$("#tablePersons").html(resultado);
				$("#message").html("Person saved successfully.");
			},
			error : function(x, y, z) {
				$("#message").html("Error saving person.");
			}
		})
	});

	$("#btnGetPermissions").click(function() {
		$.ajax({
			url : "/SpringSecurity/permission/getAll.html",
			type : "post",
			success : function(resultado) {
				$("#tablePermissions").html(resultado);
			},
			error : function(x, y, z) {
				$("#message").html("Error obtaining permissions.");
			}
		});
	});

	$("#btnSavePermission").click(function() {
		var form = $("#formPermission");
		$.ajax({
			url : form.attr('action'),
			type : form.attr('method'),
			data : form.serialize(),
			success : function(result) {
				$("#tablePermissions").html(resultado);
				$("#message").html("Permission saved successfully.");
			},
			error : function(x, y, z) {
				$("#message").html("Error saving permission.");
			}
		})
	});

	$("#btnGetRoles").click(function() {
		$.ajax({
			url : "/SpringSecurity/rol/getAll.html",
			type : "post",
			success : function(resultado) {
				$("#tableRoles").html(resultado);
			},
			error : function(x, y, z) {
				$("#message").html("Error obtaining roles.");
			}
		});
	});

	$("#btnSaveRol").click(function() {
		var form = $("#formRol");
		$.ajax({
			url : form.attr('action'),
			type : form.attr('method'),
			data : form.serialize(),
			success : function(result) {
				$("#tableRoles").html(resultado);
				$("#message").html("Rol saved successfully.");
			},
			error : function(x, y, z) {
				$("#message").html("Error saving rol.");
			}
		})
	});

	$("#btnAddRolPerson").click(function() {
		var formRoles = $("#rolesFormSelect");
		var select = $("#rolesList")[0].selectedOptions[0].value;
		var persona = $('#personId');

		$.ajax({
			url : formRoles.attr('action'),
			type : formRoles.attr('method'),
			data : {
				"rolId" : select,
				"personId" : persona.val()
			},
			success : function(result) {
				$("#tableRolesByPerson").html(result);
				$("#message").html("Rol saved successfully.");
			},
			error : function(x, y, z) {
				$("#message").html("Error saving rol to a person.");
			}
		})
	});

	// Agregar Permisos a un rol
	// Pasarlo a otro script para que haya cohesion
	$("#btnAddPermissionRol").click(function() {
		var formPermissions = $("#permissionsFormSelect");
		var select = $("#permissionsList")[0].selectedOptions[0].value;
		var rol = $('#rolId');

		$.ajax({
			url : formPermissions.attr('action'),
			type : formPermissions.attr('method'),
			data : {
				"permissionId" : select,
				"rolId" : rol.val()
			},
			success : function(result) {
				$("#tablePermissionsByRol").html(result);
				$("#message").html("Permission added to rol successfully.");
			},
			error : function(x, y, z) {
				$("#message").html("Error adding Permission to a rol.");
			}
		})
	});
}

function getPersons() {
	$.ajax({
		url : "/SpringSecurity/person/getAll.html",
		type : "post",
		success : function(resultado) {
			var persons = $("#tablePersons");
			console.log(persons);
			persons.html(resultado);
		},
		error : function(x, y, z) {
			$("#message").html("Error obtaining persons.");
		}
	});
};

function removePerson(id) {
	$.ajax({
		url : "/SpringSecurity/person/remove.html",
		type : "POST",
		data : {
			"id" : id
		},
		success : function(result) {
			$("#message").html("Person removed successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing person.");
		}
	});
}

function getPersonForm(id, action) {
	$.ajax({
		url : "/SpringSecurity/person/get.html",
		type : "POST",
		data : {
			"id" : id,
			"action" : action
		},
		success : function(result) {
			$("#formPerson").html(result);
			$("#message").html("Person obtained successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing person.");
		}
	});
}

function getRolesByPerson(personId) {
	$.ajax({
		url : "/SpringSecurity/person/getRolesByPerson.html",
		type : "POST",
		data : {
			"personId" : personId
		},
		success : function(result) {
			$("#tableRolesByPerson").html(result);
			$("#message").html("Roles by person obtained successfully.");
			startLoad();
		},
		error : function(x, y, z) {
			$("#message").html("Error obtaining permissions.");
		}
	});
}

function removePersonRol(personId, rolId) {
	$.ajax({
		url : '/SpringSecurity/person/removeRol.html',
		type : 'POST',
		data : {
			"rolId" : rolId,
			"personId" : personId
		},
		success : function(result) {
			$("#tableRolesByPerson").html(result);
			$("#message").html("Rol removed successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing rol from a person.");
		}
	});
}