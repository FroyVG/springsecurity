/**
 * 
 */

function removeRol(id) {
	$.ajax({
		url: "/SpringSecurity/rol/remove.html",
		type: "POST",
		data: {"id":id},
		success : function(result) {
			$("#message").html("Rol removed successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing rol.");
		}
	});			
}

function getRolForm(id, action){
	$.ajax({
		url: "/SpringSecurity/rol/get.html",
		type: "POST",
		data: {"id":id, "action":action},
		success : function(result) {
			$("#formRol").html(result);
			$("#message").html("Rol obtained successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing rol.");
		}
	});
}

function getPermissionsByRol(rolId){
	
	$.ajax({
		url: "/SpringSecurity/rol/getPermissionsByRol.html",
		type: "POST",
		data: {"rolId":rolId},
		success : function(result) {
			$("#formRol").html(result);
			$("#message").html("Rol obtained successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing rol.");
		}
	});
}

function removeRolPermission(rolId, permissionId){
	$.ajax({
		url: "/SpringSecurity/rol/removePermissionToRol.html",
		type: "POST",
		data: {"rolId":rolId},
		success : function(result) {
			$("#formRol").html(result);
			$("#message").html("Rol obtained successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing rol.");
		}
	});
}
