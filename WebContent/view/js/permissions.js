/**
 * 
 */

function removePermission(id) {
	$.ajax({
		url: "/SpringSecurity/permission/remove.html",
		type: "POST",
		data: {"id":id},
		success : function(result) {
			$("#message").html("Permission removed successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing permission.");
		}
	});			
}

function getPermissionForm(id, action){
	$.ajax({
		url: "/SpringSecurity/permission/get.html",
		type: "POST",
		data: {"id":id, "action":action},
		success : function(result) {
			$("#formPermission").html(result);
			$("#message").html("Permission obtained successfully.");
		},
		error : function(x, y, z) {
			$("#message").html("Error removing permission.");
		}
	});
}