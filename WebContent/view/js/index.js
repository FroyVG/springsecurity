
function cargaInicial2(){
	$("#loginForm").validationEngine();//Validación de formulario de inicio de sesión
	//JSON para traducir las opciones de la datatable a español
	var lang_ES = {"oLanguage": {
		"sProcessing" : "Procesando...",
		"sLengthMenu" : "Mostrar _MENU_ registros",
		"sZeroRecords" : "No se encontraron resultados",
		"sEmptyTable" : "Ningún dato disponible en esta tabla =(",
		"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
		"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix" : "",
		"sSearch" : "Buscar:",
		"sUrl" : "",
		"sInfoThousands" : ",",
		"sLoadingRecords" : "Cargando...",
		"oPaginate" : {
			"sFirst" : "Primero",
			"sLast" : "Último",
			"sNext" : "Siguiente",
			"sPrevious" : "Anterior"
		},
		"oAria" : {
			"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
		},
		"buttons" : {
			"copy" : "Copiar",
			"colvis" : "Visibilidad"
		}
	}};

	//$('table').dataTable($.extend(lang_ES, { buttons: [ 'copy', 'csv', 'excel', 'pdf' ] } ));
	$("#tablePersons").dataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ]
    });
	
}