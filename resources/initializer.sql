create table USUARIO
(
	ID NUMBER not null
		primary key,
	APELLIDO VARCHAR2(255 char),
	EDAD NUMBER(10),
	NOMBRE VARCHAR2(255 char),
	CORREO VARCHAR2(100),
	FECHA_NACIMIENTO DATE,
	FECHA_REGISTRO DATE,
	PASSWORD VARCHAR2(100) default '123456'
);
create or replace trigger USUARIOS_SEQ
	before insert
	on USUARIO
	for each row
begin
begin   
    select "PERSON_SEQ".nextval into :NEW.ID from dual; 
end;
end;

create table USUARIO_ROL
(
	ID NUMBER not null,
	USUARIO NUMBER not null
		constraint USUARIO_ROL_USUARIO_ID_FK
			references USUARIO,
	ROL NUMBER not null
		constraint USUARIO_ROL_ROLES_ID_FK
			references ROLES
)

comment on table USUARIO_ROL is 'Contiene los roles de cada usuario'

comment on column USUARIO_ROL.ID is 'Identificador de la tabla'

comment on column USUARIO_ROL.USUARIO is 'id del usuario'

comment on column USUARIO_ROL.ROL is 'Rol del usuario'

create unique index USUARIO_ROL_ID_UINDEX
	on USUARIO_ROL (ID)

alter table USUARIO_ROL
	add constraint USUARIO_ROL_PK
		primary key (ID)
		
create table ROL_PERMISO
(
	ID NUMBER not null,
	PERMISO NUMBER not null
		constraint ROL_PERMISO_PERMISOS_ID_FK
			references PERMISOS,
	ROL NUMBER not null
		constraint ROL_PERMISO_ROLES_ID_FK
			references ROLES
)

comment on table ROL_PERMISO is 'Contiene los permisos de cada rol'

comment on column ROL_PERMISO.ID is 'Identificador de la tabla'

comment on column ROL_PERMISO.PERMISO is 'Identificador del permiso que debe tener cada rol.'

comment on column ROL_PERMISO.ROL is 'Contiene el identificador del rol del usuario'

create unique index ROL_PERMISO_ID_UINDEX
	on ROL_PERMISO (ID)

alter table ROL_PERMISO
	add constraint ROL_PERMISO_PK
		primary key (ID)

create table PERMISOS
(
	ID NUMBER not null,
	NOMBRE VARCHAR2(50)
)

comment on table PERMISOS is 'Listado de permisos que puede tener un rol'

comment on column PERMISOS.NOMBRE is 'Nombre del permiso'

create unique index PERMISOS_ID_UINDEX
	on PERMISOS (ID)
	
alter table PERMISOS
	add constraint PERMISOS_PK
		primary key (ID);
		
create table ROLES
(
	ID NUMBER not null,
	NOMBRE VARCHAR2(50) not null
)

comment on table ROLES is 'Roles que pueden ser asignados a los usuarios'

comment on column ROLES.ID is 'Identificador de la tabla'

comment on column ROLES.NOMBRE is 'Nombre del rol.'

create unique index ROLES_ID_UINDEX
	on ROLES (ID)

alter table ROLES
	add constraint ROLES_PK
		primary key (ID)
--Modificación para quitar las llaves primarias de las tablas		
alter table USUARIO_ROL drop column ID;
--segundo
comment on column USUARIO_ROL.ID is 'Primary key'

alter table USUARIO_ROL
	add constraint USUARIO_ROL_pk
		primary key (ID);
--SpringSecurity
alter table usuario add enabled smallint;	
update usuario set enabled=1;