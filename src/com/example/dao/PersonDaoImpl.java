package com.example.dao;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Person;

@Repository
public class PersonDaoImpl implements PersonDao {
	
	private static final Logger LOG = LoggerFactory.getLogger(PersonDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HibernateTemplate template;
 
	public PersonDaoImpl() {
		System.out.println("PersonDaoImpl.PersonDaoImpl()");
	}
	@Override
	public void addPerson(Person p) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		LOG.info("Person saved successfully, Person Details="+p);
	}

	@Override
	public void updatePerson(Person p) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		try {
			session.update(p);
			tx.commit();
			LOG.info("Person updated successfully, Person Details="+p);
		} catch (Exception e) {
			LOG.error("Error updating person {}, cause {}", p, e.getMessage());
		}
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Person> listPersons() {
		
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		List<Person> personsList = new ArrayList<Person>();
		
		try {
			personsList = (List<Person>)session.createQuery("from Person").list();
			tx.commit();
		} catch (Exception e) {
			LOG.error("Can not get persons list, cause: {}", e.getCause());
			tx.rollback();
		}
		session.close();
		return personsList;
	}

	@Override
	public Person getPersonById(int id) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();		
		Person p = new Person();
		
		try {
			p= (Person) session.load(Person.class, new Integer(id));
			tx.commit();
			LOG.info("Person loaded successfully, Person details="+p);
		} catch (Exception e) {
			LOG.error("Can not obtain person with id {}, cause: {}", id, e.getCause());
			tx.rollback();
		}
		session.close();
		return p;
	}

	@Override
	public void removePerson(int id) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Person p = (Person) session.load(Person.class, new Integer(id));
		LOG.info("Person detail: {}", p.getClass());
		
		if(null != p){
			try {
				session.delete(p);
				LOG.info("Person deleted successfully, person details: {}", p);
				tx.commit();
			} catch (Exception e) {
				LOG.error("Can not remove person with id = {}, cause: {}", id, e.getCause());
				tx.rollback();
			}
		}else {
			LOG.info("Person with id = {} not found", id);
		}
		session.close();
	}
	
	@Override
	public Person getPersonByEmail(String email) {
		
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();		
		Person p = new Person();
		
		try {
			p= (Person) session.createQuery("from Person where email = :email")
						.setParameter("email", email)
						.uniqueResult();
			tx.commit();
			LOG.info("Person loaded successfully, Person details="+p);
		} catch (Exception e) {
			LOG.error("Can not obtain person with email {}, cause: {}", email, e.getCause());
			tx.rollback();
		}
		session.close();
		return p;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}