package com.example.dao;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Permission;

@Repository
public class PermissionDaoImpl implements PermissionDao {
	
	private static final Logger LOG = LoggerFactory.getLogger(PermissionDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HibernateTemplate template;

	@Override
	public void addPermission(Permission p) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		LOG.info("Permission saved successfully, Permission Details=" + p);
	}

	@Override
	public void updatePermission(Permission p) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		session.update(p);
		tx.commit();
		session.close();
		LOG.info("Permission updated successfully, Permission Details=" + p);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Permission> listPermissions() {
		
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		List<Permission> permissionList = new ArrayList<Permission>();
		
		try {
			permissionList = (List<Permission>)session.createQuery("from Permission").list();
			LOG.info("Permission List:" + permissionList);
			tx.commit();
		} catch (Exception e) {
			LOG.error("Can not get permissions list, cause: {}", e.getCause());
			tx.rollback();
		}
		session.close();
		return permissionList;
	}

	@Override
	public Permission getPermissionById(int id) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();		
		Permission p = new Permission();
		
		try {
			p= (Permission) session.load(Permission.class, new Integer(id));
			tx.commit();
			LOG.info("Permission loaded successfully, Permission details="+p);
		} catch (Exception e) {
			LOG.error("Can not obtain permission with id {}, cause: {}", id, e.getCause());
			tx.rollback();
		}
		session.close();
		return p;
	}

	@Override
	public void removePermission(int id) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Permission p = (Permission) session.load(Permission.class, new Integer(id));
		LOG.info("Permission detail: {}", p.getClass());
		
		if(null != p){
			try {
				session.delete(p);
				LOG.info("Permission deleted successfully, permission details: {}", p);
				tx.commit();
			} catch (Exception e) {
				LOG.error("Can not remove permission with id = {}, cause: {}", id, e.getCause());
				tx.rollback();
			}
		}else {
			LOG.info("Permission with id = {} not found", id);
		}
		session.close();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}