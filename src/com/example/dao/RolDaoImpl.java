package com.example.dao;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Rol;

@Repository
public class RolDaoImpl implements RolDao {
	
	private static final Logger LOG = LoggerFactory.getLogger(RolDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private HibernateTemplate template;

	@Override
	public void addRol(Rol p) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		session.save(p);
		tx.commit();
		session.close();
		LOG.info("Rol saved successfully, Rol Details=" + p);
	}

	@Override
	public void updateRol(Rol p) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		session.update(p);
		tx.commit();
		session.close();
		LOG.info("Rol updated successfully, Rol Details=" + p);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Rol> listRol() {
		
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		List<Rol> rolList = new ArrayList<Rol>();
		
		try {
			rolList = (List<Rol>)session.createQuery("from Rol").list();
			LOG.info("Rol List:" + rolList);
			tx.commit();
		} catch (Exception e) {
			LOG.error("Can not get rol list, cause: {}, message: {}", e.getCause(), e.getMessage());
			tx.rollback();
		}
		session.close();
		return rolList;
	}

	@Override
	public Rol getRolById(int id) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();		
		Rol p = new Rol();
		
		try {
			p= (Rol) session.load(Rol.class, new Integer(id));
			tx.commit();
			LOG.info("Rol loaded successfully, Rol details="+p);
		} catch (Exception e) {
			LOG.error("Can not obtain rol with id {}, cause: {}", id, e.getCause());
			tx.rollback();
		}
		session.close();
		return p;
	}

	@Override
	public void removeRol(int id) {
		Session session = this.template.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		Rol p = (Rol) session.load(Rol.class, new Integer(id));
		LOG.info("Rol detail: {}", p.getClass());
		
		if(null != p){
			try {
				session.delete(p);
				LOG.info("Rol deleted successfully, rol details: {}", p);
				tx.commit();
			} catch (Exception e) {
				LOG.error("Can not remove rol with id = {}, cause: {}", id, e.getCause());
				tx.rollback();
			}
		}else {
			LOG.info("Rol with id = {} not found", id);
		}
		session.close();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}