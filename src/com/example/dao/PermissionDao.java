package com.example.dao;

import java.util.List;

import com.example.model.Permission;

public interface PermissionDao{

	public void addPermission(Permission p);
	public void updatePermission(Permission p);
	public List<Permission> listPermissions();
	public Permission getPermissionById(int id);
	public void removePermission(int id);
}