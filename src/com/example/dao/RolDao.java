package com.example.dao;

import java.util.List;

import com.example.model.Rol;

public interface RolDao{

	public void addRol(Rol p);
	public void updateRol(Rol p);
	public List<Rol> listRol();
	public Rol getRolById(int id);
	public void removeRol(int id);
}