package com.example.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.Person;
import com.example.model.Rol;
import com.example.service.PersonService;
import com.example.service.PersonServiceImpl;
import com.example.service.RolService;

@Controller
@RequestMapping(value = "/person")
@Transactional
public class PersonController {
	private static final Logger LOG = LoggerFactory.getLogger(PersonController.class.getName());

	@Autowired // (required=true)
	private PersonService personService;
	
	@Autowired
	private RolService rolService;

	@Qualifier(value = "personService")
	public void setPersonService(PersonService ps) {
		this.personService = ps;
	}
	public PersonService getPersonService() {
		return personService;
	}

	public RolService getRolService() {
		return rolService;
	}
	public void setRolService(RolService rolService) {
		this.rolService = rolService;
	}

	@RequestMapping(value = "/getAll.html", method = RequestMethod.POST)
	public String listPersons(Model model) {
		LOG.info("PersonController.listPersons()");
		model.addAttribute("listPersons", this.personService.listPersons());
		return "persons";
	}

	// For add and update person both
	@RequestMapping(value = "/add.html", method = RequestMethod.POST)
	public String addPerson(Model model, @ModelAttribute("person") Person person, BindingResult bindingResult) {
		LOG.debug(person.toString());
		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			LOG.error(fieldError.getField() + " : " + fieldError.getDefaultMessage());
		}

		person.setRegisteredDate(new Date());
		person.setPassword("123456");
		if (person.getId() == null) {
			// new person, add it
			this.personService.addPerson(person);
			LOG.info("Person added successfully, person detail: {}", person);
		} else {
			// existing person, call update
			this.personService.updatePerson(person);
			LOG.info("Person modified successfully, person detail: {}", person);
		}

		return listPersons(model);
	}

	@RequestMapping(value="/remove.html")
	public String removePerson(@RequestParam("id") Integer id, Model model) {
		LOG.info("/person/remove.html");
		this.personService.removePerson(id);
		model.addAttribute("message", "Person removed successfully");
		return listPersons(model);
	}

	@RequestMapping(value="/edit.html")
	public String editPerson(Model model, @ModelAttribute("person") Person person, BindingResult bindingResult) {
		LOG.info("/person/edit.html");
		this.personService.updatePerson(person);
		model.addAttribute("message", "Person modified successfully");
		model.addAttribute("listPersons", this.personService.listPersons());
		return listPersons(model);
	}

	@RequestMapping(value="/get.html")
	public String getPerson(@RequestParam("id") Integer id, @RequestParam("action") String action , Model model) {
		LOG.info("/person/get.html, personId={}, action={}", id, action);
		Person person;
		
		if (id == null) {
			person = new Person();
		} else {
			person = this.personService.getPersonById(id);
		}
		model.addAttribute("action", action);
		model.addAttribute("person", person);
		return "personForm";
	}
	
	@RequestMapping(value="/addRol.html")
	public String addRol(@RequestParam("personId") Integer personId, @RequestParam("rolId") Integer rolId, Model model) {
		
		Person person = this.personService.getPersonById(personId);
		Rol rol = this.rolService.getRolById(rolId);
		person.addRoles(rol);
		this.personService.updatePerson(person);
		return getRolesByPerson(personId, model);
	}
	
	@RequestMapping(value="/getRolesByPerson.html") 
	public String getRolesByPerson( @RequestParam("personId") Integer personId, Model model){
		model.addAttribute("rolesByPerson", this.personService.getPersonById(personId).getRoles());
		model.addAttribute("roles", this.rolService.listRol());
		model.addAttribute("person", this.personService.getPersonById(personId));
		return "personRoles";
	}
	
	@RequestMapping(value="/removeRol.html")
	public String removeRol(@RequestParam("personId") Integer personId, @RequestParam("rolId") Integer rolId, Model model) {
		
		Person person = this.personService.getPersonById(personId);
		Rol rol = this.rolService.getRolById(rolId);
		person.removeRoles(rol);
		LOG.info("Person after rol removed: {}", person);
		this.personService.updatePerson(person);
		LOG.info("Person updated: {}", person);
		return getRolesByPerson(personId, model);
	} 

	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
}