package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.Permission;
import com.example.model.Rol;
import com.example.service.PermissionService;
import com.example.service.RolService;

@Controller
@RequestMapping(value = "/rol")
public class RolController {
	private static final Logger LOG = LoggerFactory.getLogger(RolController.class.getName());

	@Autowired // (required=true)
	private RolService rolService;
	
	@Autowired
	private PermissionService permissionService;

	@Qualifier(value = "rolService")
	public void setRolService(RolService rs) {
		this.rolService = rs;
	}

	@RequestMapping(value = "/load.html", method = RequestMethod.GET)
	public String load(HttpServletRequest request) {
		LOG.info("RequestURI: " + request.getRequestURI());
		request.setAttribute("rol", new Rol());
		return "index";
	}

	@RequestMapping(value = "/getAll.html", method = RequestMethod.POST)
	public String listRol(Model model) {
		LOG.debug(model.toString());
		LOG.info("RolController.listRol()");
		model.addAttribute("listRoles", this.rolService.listRol());
		return "roles";
	}

	//For add and update rol
	@RequestMapping(value = "/add.html", method = RequestMethod.POST)
	public String addRol(Model model, @ModelAttribute("rol") Rol rol, BindingResult bindingResult) {
		LOG.debug(rol.toString());
		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			LOG.error(fieldError.getField() + " : " + fieldError.getDefaultMessage());
		}

		if (rol.getId() == null) {
			// new rol, add it
			this.rolService.addRol(rol);
			LOG.info("Rol added successfully, rol detail: {}", rol);
		} else {
			// existing rol, call update
			this.rolService.updateRol(rol);
			LOG.info("Rol modified successfully, rol detail: {}", rol);
		}

		return listRol(model);
	}

	@RequestMapping("/remove.html")
	public String removeRol(@RequestParam("id") Integer id, Model model) {
		LOG.info("/rol/remove.html");
		this.rolService.removeRol(id);
		model.addAttribute("message", "Rol removed successfully");
		return listRol(model);
	}

	@RequestMapping("/edit.html")
	public String editRol(Model model, @ModelAttribute("rol") Rol rol, BindingResult bindingResult) {
		LOG.info("/rol/edit.html");
		this.rolService.updateRol(rol);
		model.addAttribute("message", "Rol modified successfully");
		model.addAttribute("listRol", this.rolService.listRol());
		return listRol(model);
	}

	@RequestMapping("/get.html")
	public String getRol(@RequestParam("id") Integer id, @RequestParam("action") String action , Model model) {
		LOG.info("/rol/get.html, rolId={}, action={}", id, action);
		Rol rol;
		
		if (id == null) {
			rol = new Rol();
		} else {
			rol = this.rolService.getRolById(id);
		}
		model.addAttribute("action", action);
		model.addAttribute("rol", rol);
		return "rolForm";
	}
	
	@RequestMapping(value="/getPermissionsByRol.html", method=RequestMethod.POST) 
	public String getRolesByPerson( @RequestParam("rolId") Integer rolId, Model model){
		model.addAttribute("permissionsByRol", this.rolService.getRolById(rolId).getPermissions());
		model.addAttribute("permissions", this.permissionService.listPermissions());
		model.addAttribute("rol", this.rolService.getRolById(rolId));
		return "rolesPermissions";
	}
	
	@RequestMapping("/removePermissionToRol.html")
	public String removePermissionToRol(@RequestParam("rolId") Integer rolId, @RequestParam("permissionId") Integer permissionId, Model model) {
		LOG.info("/rol/removePermissionToRol.html");
		Rol rol = this.rolService.getRolById(rolId);
		rol.getPermissions().remove(permissionId);
		this.rolService.updateRol(rol);
		model.addAttribute("message", "Permission removed from  successfully");
		return getRolesByPerson(rolId, model);
	}
	
	@RequestMapping("/addPermissionToRol.html")
	public String addPermissionToRol(@RequestParam("permissionId") Integer permissionId, @RequestParam("rolId") Integer rolId, Model model) {
		Rol rol = this.rolService.getRolById(rolId);
		Permission permission = this.permissionService.getPermissionById(permissionId);
		rol.getPermissions().add(permission);
		this.rolService.updateRol(rol);
		return getRolesByPerson(rolId, model);	
	}
}