package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.model.LoginDto;
import com.example.model.Person;
import com.example.service.PersonService;

@Controller
public class LoginController {
	private static final Logger LOG = LoggerFactory.getLogger(PersonController.class.getName());
	
	@Autowired // (required=true)
	private PersonService personService;

	@Qualifier(value = "personService")
	public void setPersonService(PersonService ps) {
		this.personService = ps;
	}
	public PersonService getPersonService() {
		return personService;
	}
	
	@RequestMapping(value="/login.html", method=RequestMethod.GET)
	public String login(HttpServletRequest request, @RequestParam(value = "error", required = false) String error,@RequestParam(value = "logout", required = false) String logout, Model model) {
		LOG.info("LoginController.login()");
		if (error != null) {
			model.addAttribute("error", "Invalid username or password!");
		}
		if (logout != null) {
			model.addAttribute("msg", "You've been logged out successfully.");
		}
		request.setAttribute("loginDto", new LoginDto());
		return "login";
	}
	
	@RequestMapping(value = "/welcome.html", method = RequestMethod.GET)
	public ModelAndView welcome(@ModelAttribute("loginDto") LoginDto loginDto) {
		ModelAndView model = new ModelAndView();
		User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		LOG.info("LoginController.welcome(), user logged: {}", principal.getUsername());
		Person user = personService.getPersonByEmail(principal.getUsername());
		model.addObject("user",user);
		
		if (!principal.isAccountNonExpired()&& !principal.isEnabled() ) {
			model.addObject("error", "Expired session or principal isn�t enabled");
			model.setViewName("login");
		}else {
			model.setViewName("index");
		}
		return model;
	}
	
	//for 403 access denied page
	@RequestMapping(value = "/403.html", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();
		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			LOG.info("user logged userName: {}, authorithies: {}, ", userDetail.getUsername(), userDetail.getAuthorities());
			model.addObject("username", userDetail.getUsername());
		}
		model.setViewName("denied");
		return model;
	}
 
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model) {
        return "login";
    }
	
}
