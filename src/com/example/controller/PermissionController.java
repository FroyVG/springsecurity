package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.Permission;
import com.example.service.PermissionService;

@Controller
@RequestMapping(value = "/permission")
public class PermissionController {
	private static final Logger LOG = LoggerFactory.getLogger(PermissionController.class.getName());

	@Autowired // (required=true)
	private PermissionService permissionService;

	@Qualifier(value = "permissionService")
	public PermissionService getPermissionService() {
		return permissionService;
	}

	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}

	@RequestMapping(value = "/load.html", method = RequestMethod.GET)
	public String load(HttpServletRequest request) {
		LOG.info("RequestURI: " + request.getRequestURI());
		request.setAttribute("permission", new Permission());
		return "index";
	}

	@RequestMapping(value = "/getAll.html", method = RequestMethod.POST)
	public String listPermissions(Model model) {
		LOG.debug(model.toString());
		LOG.info("PermissionController.listPermissions()");
		model.addAttribute("listPermissions", this.permissionService.listPermissions());
		return "permissions";
	}

	//For add and update permission
	@RequestMapping(value = "/add.html", method = RequestMethod.POST)
	public String addPermission(Model model, @ModelAttribute("permission") Permission permission, BindingResult bindingResult) {
		LOG.debug(permission.toString());
		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			LOG.error(fieldError.getField() + " : " + fieldError.getDefaultMessage());
		}

		if (permission.getId() == null) {
			// new permission, add it
			this.permissionService.addPermission(permission);
			LOG.info("Permission added successfully, permission detail: {}", permission);
		} else {
			// existing permission, call update
			this.permissionService.updatePermission(permission);
			LOG.info("Permission modified successfully, permission detail: {}", permission);
		}

		return listPermissions(model);
	}

	@RequestMapping("/remove.html")
	public String removePermission(@RequestParam("id") Integer id, Model model) {
		LOG.info("/permission/remove.html");
		this.permissionService.removePermission(id);
		model.addAttribute("message", "Permission removed successfully");
		return listPermissions(model);
	}

	@RequestMapping("/edit.html")
	public String editPermission(Model model, @ModelAttribute("permission") Permission permission, BindingResult bindingResult) {
		LOG.info("/permission/edit.html");
		this.permissionService.updatePermission(permission);
		model.addAttribute("message", "Permission modified successfully");
		model.addAttribute("listPermissions", this.permissionService.listPermissions());
		return listPermissions(model);
	}

	@RequestMapping("/get.html")
	public String getPermission(@RequestParam("id") Integer id, @RequestParam("action") String action , Model model) {
		LOG.info("/permission/get.html, permissionId={}, action={}", id, action);
		Permission permission;
		
		if (id == null) {
			permission = new Permission();
		} else {
			permission = this.permissionService.getPermissionById(id);
		}
		model.addAttribute("action", action);
		model.addAttribute("permission", permission);
		return "permissionForm";
	}

}