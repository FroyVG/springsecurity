package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.RolDaoImpl;
import com.example.model.Rol;

@Service
public class RolServiceImpl implements RolService {
	
	@Autowired
	private RolDaoImpl rolDAO;


	@Override
	public void addRol(Rol p) {
		this.rolDAO.addRol(p);
	}

	@Override
	public void updateRol(Rol p) {
		this.rolDAO.updateRol(p);
	}

	@Override
	public List<Rol> listRol() {
		return this.rolDAO.listRol();
	}

	@Override
	public Rol getRolById(int id) {
		return this.rolDAO.getRolById(id);
	}

	@Override
	public void removeRol(int id) {
		this.rolDAO.removeRol(id);
	}

	public RolDaoImpl getRolDAO() {
		return rolDAO;
	}

	public void setRolDAO(RolDaoImpl rolDAO) {
		this.rolDAO = rolDAO;
	}

}