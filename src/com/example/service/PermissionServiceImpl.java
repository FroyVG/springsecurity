package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PermissionDao;
import com.example.dao.PermissionDaoImpl;
import com.example.model.Permission;

@Service
public class PermissionServiceImpl implements PermissionService {
	
	@Autowired
	private PermissionDao permissionDAO;


	@Override
	public void addPermission(Permission p) {
		this.permissionDAO.addPermission(p);
	}

	@Override
	public void updatePermission(Permission p) {
		this.permissionDAO.updatePermission(p);
	}

	@Override
	public List<Permission> listPermissions() {
		return this.permissionDAO.listPermissions();
	}

	@Override
	public Permission getPermissionById(int id) {
		return this.permissionDAO.getPermissionById(id);
	}

	@Override
	public void removePermission(int id) {
		this.permissionDAO.removePermission(id);
	}

	public PermissionDao getPermissionDAO() {
		return permissionDAO;
	}

	public void setPermissionDAO(PermissionDaoImpl permissionDAO) {
		this.permissionDAO = permissionDAO;
	}

}