package com.example.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;

@Entity
@Table(name = "usuario")
@Transactional // Para solventar el error: El Servlet.service() para el servlet [spring] en el
				// contexto con ruta [/SpringSecurity] lanz� la excepci�n [Request processing
				// failed; nested exception is org.hibernate.LazyInitializationException: failed
				// to lazily initialize a collection of role: com.example.model.Rol.persons, no
				// session or session was closed]
public class Person implements Serializable {

	private static final long serialVersionUID = -4844827147461316975L;
	private Long id;
	private String name;
	private String firstName;
	private String email;
	private Integer age;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date birthDate;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date registeredDate;
	private String password;

	private Set<Rol> roles = new HashSet<Rol>();

	public Person() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void removeRoles(Rol rol) {
		roles.remove(rol);
		rol.getPersons().remove(this);
	}

	public void addRoles(Rol rol) {
		roles.add(rol);
		rol.getPersons().add(this);
	}

	@ManyToMany(targetEntity= Rol.class, cascade = CascadeType.ALL, fetch= FetchType.EAGER)
	@JoinTable(name = "usuario_rol", 
				joinColumns = { @JoinColumn(name = "usuario") }, 
				inverseJoinColumns = {@JoinColumn(name = "rol") })
	public Set<Rol> getRoles() {
		return roles;
	}

	public void setRoles(Set<Rol> roles) {
		this.roles = roles;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "apellido")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "correo")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "edad")
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "fecha_nacimiento")
	// @DateTimeFormat( iso=ISO.DATE, pattern = "yyyy-MM-dd")
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "fecha_registro")
	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", firstName=" + firstName + ", email=" + email + ", age=" + age
				+ ", birthDate=" + birthDate + ", registeredDate=" + registeredDate + ", password=" + password
				+ ", roles=" + roles + "]";
	}

}
