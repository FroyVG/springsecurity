package com.example.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.transaction.annotation.Transactional;
 
@Entity
@Table(name="roles")
@Transactional
public class Rol implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name; 
	
	private Set<Person> persons = new HashSet<Person>();
	private Set<Permission> permissions = new HashSet<Permission>();
	
	public Rol() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void removePermissions(Permission permission) {
		permissions.remove(permission);
		permission.getRoles().remove(this);
	}
	
	public void addPermissions(Permission permission) {
		permissions.add(permission);
		permission.getRoles().add(this);
	}

	@ManyToMany(targetEntity=Permission.class, cascade=CascadeType.ALL, fetch= FetchType.EAGER)
	@JoinTable(name="rol_permiso", 
				joinColumns= {@JoinColumn(name="rol")}, 
				inverseJoinColumns= {@JoinColumn(name = "permiso")})
	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	@ManyToMany(cascade = CascadeType.ALL, mappedBy="roles", targetEntity=Person.class)
	public Set<Person> getPersons() {
		return persons;
	}
	
	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}

	@Column(name="nombre")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Rol [id=" + id + ", name=" + name + "]";
	}
	
}
